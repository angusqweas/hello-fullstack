import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const useLogin = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const navigate = useNavigate();

  const checkLoginStatus = async () => {
    try {
      const token = localStorage.getItem('token');
      if (!token) {
        throw new Error('No token found');
      }

      const response = await fetch('/auth', {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      });

      if (response.ok) {
        setIsLoggedIn(true);
        return true;
      } else {
        throw new Error('Not authenticated');
      }
    } catch (error) {
      setIsLoggedIn(false);
      navigate('/login'); // 如果未登入，重定向到登入頁面
      return false;
    }
  };

  const handleLogin = async (event, username, password) => {
    event.preventDefault();
    try {
      const response = await fetch('http://localhost:8000/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username, password }),
      });

      if (response.ok) {
        const data = await response.json();
        localStorage.setItem('token', data.access_token); // 存储 token
        navigate('/'); // 登录成功后重定向到首页
      } else {
        console.error('Login failed');
      }
    } catch (error) {
      console.error('Login failed', error);
    }
  };

  const handleRegister = async (event, username, password) => {
    event.preventDefault();
    const response = await fetch('http://localhost:8000/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ username, password })
    });

    const data = await response.json();
    if (response.status === 200) {
      alert('Registration successful');
    } else {
      alert(data.detail || 'An error occurred during registration');
    }
  };

  return {
    checkLoginStatus,
    handleLogin,
    handleRegister,
    isLoggedIn
  };
};

export default useLogin;
