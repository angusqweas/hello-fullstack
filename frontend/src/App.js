import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Login from './pages/Login/Login';
import HomePage from './pages/HomePage/HomePage';

function App() {
  const [helloData, setHelloData] = React.useState('');
  const [usersData, setUsersData] = React.useState([]);

  const fetchHello = async () => {
    fetch('http://localhost:8000/')
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setHelloData(data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const fetchData = async () => {
    fetch('http://localhost:8000/users')
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setUsersData(data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  React.useEffect(() => {
    fetchHello();
    fetchData();
  }, []);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" exact element={<Login />} />
        <Route path="/" exact element={<HomePage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
