import React from 'react'
import TaiwanMap from '../TaiwanMap/TaiwanMap'

const HomePage = () => {
  return (
    <TaiwanMap />
  )
}

export default HomePage
