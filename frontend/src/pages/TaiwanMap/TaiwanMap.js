import React from 'react'
import Taiwan from "@svg-maps/taiwan";
import { SVGMap } from "react-svg-map";
import "react-svg-map/lib/index.css";
import "./TaiwanMap.css";
import useLogin from '../../hooks/useLogin';

const TaiwanMap = () => {
  const { checkLoginStatus, isLoggedIn } = useLogin();

  const [selectedLocationData, setSelectedLocationData] = React.useState([
    {
      id: "taoyuan-city",
      residenceDuration: 2,
    },
    {
      id: "hualien-county",
      residenceDuration: 3,
    },
    {
      id: "taichung-city",
      residenceDuration: 1,
    },
  ]);

  const handleLocationClick = (location) => {
    // 如果未登入，則不能點選地方
    if (!isLoggedIn && !checkLoginStatus()) return;

    // 如果選中的id不在selectedLocationData中，則新增一筆
    if (!selectedLocationData.find((item) => item.id === location.target.getAttribute("id"))) {
      setSelectedLocationData([...selectedLocationData, { id: location.target.getAttribute("id"), residenceDuration: 1 }]);
    } else {
      // 否則，residencyDuration加1，若residencyDuration大於4，則置0
      const index = selectedLocationData.findIndex((item) => item.id === location.target.getAttribute("id"));
      const newResidenceDuration = selectedLocationData[index].residenceDuration + 1;
      if (newResidenceDuration > 4) {
        setSelectedLocationData([...selectedLocationData.slice(0, index), { id: location.target.getAttribute("id"), residenceDuration: 0 }]);
      } else {
        setSelectedLocationData([...selectedLocationData.slice(0, index), { id: location.target.getAttribute("id"), residenceDuration: newResidenceDuration }]);
      }
    }
  };

  React.useEffect(() => {
    const durationMap = new Map([
      ["0", "#a1d99b"],
      ["1", "red"],
      ["2", "orange"],
      ["3", "yellow"],
      ["4", "green"],
    ]);

    selectedLocationData.map((item) => {
      document.getElementById(item.id).style.fill = durationMap.get(item.residenceDuration.toString());
    });

    // 隱藏特定的地方
    document.getElementById("lienchiang-county").style.display = "none";
    document.getElementById("kinmen-county").style.display = "none";
  }, [selectedLocationData]);

  return (
    <>
      <div className="w-1/2">
        <SVGMap
          map={Taiwan}
          onLocationClick={handleLocationClick}
        />
      </div>
    </>
  )
}

export default TaiwanMap
