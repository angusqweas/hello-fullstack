import React, { useState } from 'react';
import useLogin from '../../hooks/useLogin';

function Login() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const { handleLogin, handleRegister } = useLogin();

  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  return (
    <div className="bg-gray-100 p-4 rounded-lg w-64 mx-auto my-2 absolute top-2 left-1/2 -translate-x-1/2 text-center">
      <h2 className="text-xl font-bold">Login / Register</h2>
      <form>
        <label className="block text-gray-700" htmlFor="username">
          <span className="text-gray-500">Username:</span>
          <input type="text" value={username} onChange={handleUsernameChange} required />
        </label>
        <br />
        <label className="block text-gray-700" htmlFor="password">
          <span className="text-gray-500">Password:</span>
          <input type="password" value={password} onChange={handlePasswordChange} required />
        </label>
        <br />
        <button onClick={(e) => handleRegister(e, username, password)} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ml-2">
          Register
        </button>
        <button onClick={(e) => handleLogin(e, username, password)} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ml-2">
          Login
        </button>
      </form>
    </div>
  );
}

export default Login;
