# auth.py
from fastapi import APIRouter, HTTPException, status, Depends
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext
from pydantic import BaseModel
import sqlite3

router = APIRouter()

# 使用 passlib 來進行密碼加密和驗證
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

# OAuth2 密碼 bearer token
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

# 使用者模型
class User(BaseModel):
    username: str
    password: str

# 登入請求模型
class LoginRequest(BaseModel):
    username: str
    password: str

# 驗證密碼的函數
def verify_password(plain_password, password):
    return pwd_context.verify(plain_password, password)

# 驗證使用者
def authenticate_user(username: str, password: str):
    try:
        # 連接到 SQLite 資料庫
        connection = sqlite3.connect("mydatabase.db")
        cursor = connection.cursor()

        # 獲取使用者信息
        cursor.execute("SELECT * FROM users WHERE username = ?", (username,))
        user = cursor.fetchone()

        # 關閉資料庫連接
        connection.close()

        if user and pwd_context.verify(password, user[2]):  # user[2] 是 password 欄位
            return {"username": user[1], "password": user[2], "disabled": user[3]}  # user[1] 是 username 欄位，user[3] 是 disabled 欄位
        else:
            return False
    except sqlite3.Error as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=str(e)
        )

# 註冊新使用者
def create_user(user: User):
    try:
        # 連接到 SQLite 資料庫
        connection = sqlite3.connect("mydatabase.db")
        cursor = connection.cursor()

        # 檢查使用者是否已存在
        cursor.execute("SELECT * FROM users WHERE username = ?", (user.username,))
        if cursor.fetchone():
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Username already registered"
            )

        # 加密密碼
        hashed_password = pwd_context.hash(user.password)

        # 插入新用戶資料
        cursor.execute(
            "INSERT INTO users (username, password, disabled) VALUES (?, ?, ?)",
            (user.username, hashed_password, False)
        )

        # 提交變更並關閉資料庫連接
        connection.commit()
        connection.close()

        return user
    except sqlite3.Error as e:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=str(e)
        )

# 註冊端點
@router.post("/register", response_model=User)
async def register(user: User):
    return create_user(user)

# 登入端點
@router.post("/login")
async def login_for_access_token(login_request: LoginRequest):
    user = authenticate_user(login_request.username, login_request.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    # 返回 token
    return {"access_token": user["password"], "token_type": "bearer"}

# 獲取用戶信息
def get_user_by_username(username: str):
    try:
        # 連接到 SQLite 資料庫
        connection = sqlite3.connect("mydatabase.db")
        cursor = connection.cursor()

        # 獲取使用者信息
        cursor.execute("SELECT * FROM users WHERE username = ?", (username,))
        user = cursor.fetchone()

        # 關閉資料庫連接
        connection.close()

        if user:
            return {"username": user[1], "disabled": user[3]}  # user[1] 是 username 欄位，user[3] 是 disabled 欄位
        else:
            return None
    except sqlite3.Error as e:
        raise HTTPException(
            status_code=500,
            detail=str(e)
        )

# 受保護的端點
@router.get("/auth")
async def read_users_me(token: str = Depends(oauth2_scheme)):
    user = get_user_by_username(token)
    if not user:
        raise HTTPException(
            status_code=404,
            detail="User not found"
        )
    return user

