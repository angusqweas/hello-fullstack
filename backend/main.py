# main.py
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import sqlite3
from auth import router as auth_router

app = FastAPI()

# 解決跨域問題
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# 導入 auth 路由
app.include_router(auth_router)

@app.get("/")
def read_root():
    return {"message": "Hello, World!"}

@app.get("/users")
async def get_users():
    try:
        # 連接到 SQLite 資料庫
        connection = sqlite3.connect("mydatabase.db")
        cursor = connection.cursor()

        # 執行 SQL 查詢，擷取所有用戶
        cursor.execute("SELECT * FROM users")
        users = cursor.fetchall()

        # 關閉資料庫連接
        connection.close()

        return {"users": users}
    except sqlite3.Error as e:
        return {"error": str(e)}
